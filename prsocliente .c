#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>

int Abre_Conexion_Unix (char *servicio)
{
	struct sockaddr_un direccion;
	int Descriptor;

	strcpy (direccion.sun_path, servicio);
	direccion.sun_family = AF_UNIX;

	Descriptor = socket (AF_UNIX, SOCK_STREAM, 0);
	if (Descriptor == -1)
		return -1;
	if (connect (
			Descriptor,
			(struct sockaddr *)&direccion,
			strlen (direccion.sun_path) + sizeof (direccion.sun_family)) == -1)
	{
		return -1;
	}
	else
	{
		return Descriptor;
	}
}

int Abre_Conexion_Inet (
	char *Host_Servidor,
	char *Servicio)
{
	struct sockaddr_in Direccion;
	struct servent *Puerto;
	struct hostent *Host;
	int Descriptor;

	Puerto = getservbyname (Servicio, "T C P");
	if (Puerto == NULL)
		return -1;

	Host = gethostbyname (Host_Servidor);
	if (Host == NULL)
		return -1;

Direccion.sin_family = AF_INET;
Direccion.sin_addr.s_addr = ((struct in_addr *)(Host->h_addr))->s_addr;
Direccion.sin_port = Puerto->s_port;
	Descriptor = socket (AF_INET, SOCK_STREAM, 0);
	if (Descriptor == -1)
		return -1;

	if (connect (
			Descriptor,
			(struct sockaddr *)&Direccion,
			sizeof (Direccion)) == -1)
	{
		return -1;
	}

	return Descriptor;
}
